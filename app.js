﻿/// <reference path="typings/node/node.d.ts"/>
/// <reference path="typings/express/express.d.ts"/>
/// <reference path="typings/socket.io/socket.io.d.ts"/>
/// <reference path="typings/nedb/nedb.d.ts"/>

var port = process.env.port || 8000;
var express = require('express');
var ExpressPeerServer = require('peer').ExpressPeerServer;
var jwt_secret = "asfgdfhfs2353456dfgfdh";
var jwt = require('jsonwebtoken');
var bodyParser = require('body-parser');
var socketioJwt   = require("socketio-jwt");
var Datastore = require('nedb'), db = new Datastore({ filename: 'users', autoload: true });
var https = require('https');
var path = require('path');
var compress = require('compression');

db.ensureIndex({ fieldName: 'login', unique: true });
db.ensureIndex({ fieldName: 'password' });
db.persistence.setAutocompactionInterval(43200000); // 12 hours

var app = express();
app.use(compress());

var consts = {
	MoreSymbolsInPassword: 'Пожалуйста, добавьте больше символов в пароль',
	IncorrectPasswordOrLogin: 'Неправильные пароль или логин',
	MoreSymbolsInLogin: 'Логин должен содержать от 3 до 10 символов',
	SelectSex: 'Пожалуйста, выберите пол',
	OnlyNumbersOrLetters: "Пожалуйста, вводите только латинские буквы и цифры в поле логина",
	LessSymbolsInPassword: "Пожалуйста, введите в поле пароля до 16 символов",
	WrongCaptcha: "Неправильная каптча",
	InternalError: "Ошибка сервера. К сожалению, сайт работает некорректно. Попробуйте пожаловаться через страницу продукта - https://plus.google.com/110495113357312810707/posts/jfV2NSz2ohq"
};

app.use(express.static(path.join(__dirname, 'src')));
app.use(express.static(path.join(__dirname,'bower_components/jquery/dist')));
app.use(express.static(path.join(__dirname,'bower_components/bootstrap/dist')));
app.use(express.static(path.join(__dirname,'bower_components/peerjs')));
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', function(req, res){
    res.sendFile('index.html', { root: __dirname + "/src" } );
});

function verifyRecaptcha(key, ip, callback) 
{
	https.get("https://www.google.com/recaptcha/api/siteverify?secret=6LcA7AUTAAAAALDH2koNWXEG_HnHl7rEui-7OyU1&response=" + key, function(res) {
		var data = "";
		res.on('data', function (chunk) { data += chunk.toString(); }); 
		res.on('end', function() { 
			try { 
				var parsedData = JSON.parse(data);
				callback(parsedData.success); 
			} catch (e) {
				callback(false);
			} 
		});
	});
}

app.use(bodyParser.urlencoded({ extended: false }));
var letters = /^[0-9a-zA-Z]+$/;
app.post('/login', function (req, res) {
  	if (!req.body) return res.status(400).json('Bad request');
	verifyRecaptcha(req.body["recaptcha"], req.ip, function(success)
	{
		if (!success)
		{
			return res.status(400).json({error: consts.WrongCaptcha});
		}
		var profile = {
			login: req.body.login,
			password:  req.body.password,
			sex:  req.body.sex,
			id: req.body.id
		};
	
		if (profile.password.length < 4)
			return res.status(400).json({error: consts.MoreSymbolsInPassword});
		if (profile.password.length > 16)
			return res.status(400).json({error: consts.LessSymbolsInPassword});
		if (profile.login.length > 10 || profile.login.length < 3)
			return res.status(400).json({error: consts.MoreSymbolsInLogin});
		if (!profile.login.match(letters))
			return res.status(400).json({error: consts.OnlyNumbersOrLetters});
		db.find({login:profile.login, password: profile.password}, function(err, profiles)
		{
			if (!err)
			{
				if (profiles.length == 1)
				{
					profile.sex = profiles[0].sex;
					db.update({login:profile.login, password: profile.password}, {$set: { last: new Date()}});
					return res.json({token: jwt.sign(profile, jwt_secret, {expiresInMinutes: 60}), sex: profiles[0].sex});
				}
				else {
					db.find({login:profile.login}, function(err2, profiles2)
					{
						if (!err2)
						{
							if (profiles2.length > 0)
							{
								return res.status(400).json({error: consts.IncorrectPasswordOrLogin});
							}
							else{
								if (!profile.sex || (profile.sex !== 'm' && profile.sex !== 'w'))
									return res.status(400).json({error: consts.SelectSex});
								profile.created = new Date();
								db.insert(profile);
								return res.json({token: jwt.sign(profile, jwt_secret, {expiresInMinutes: 60}), sex: profile.sex});
							}
						}
						else {
							return res.status(500).json({error: consts.InternalError});
						}
					});
				}
			}
			else {
				return res.status(500).json({error: consts.InternalError});
			}
		});
	});
});

var server = app.listen(port);

var options = {
	proxied: true
};

app.use('/peers-api', ExpressPeerServer(server, options));

var io = require("socket.io")(server);

io.use(socketioJwt.authorize({
  secret: jwt_secret,
  handshake: true
}));

io.on('connection', function (socket) {
	var user = socket.decoded_token;
  	socket.on("room", function(message) {
		if (socket.room !== undefined) {
			// Если сокет уже находится в какой-то комнате, выходим из нее
		    socket.leave(socket.room);
		} 
		// Входим в запрошенную комнату
		socket.room = message.room;
		socket.join(socket.room);
		socket.user_id = user.id;
		// Отправялем остальным клиентам в этой комнате сообщение о присоединении нового участника
		socket.broadcast.to(socket.room).emit("new", {id: user.id, login: user.login, sex: user.sex});
	});
});
