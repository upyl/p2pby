﻿/// <reference path="../../typings/jquery/jquery.d.ts"/>
/// <reference path="../../typings/socket.io/socket.io.d.ts"/>
/// <reference path="../../typings/peerjs/peerjs.d.ts"/>

var chatlog = $("#chat-messages");
var message = document.getElementById("message");
var send_btn = document.getElementById("send-btn");
var custom_room = document.getElementById("custom_room");
var usersContainer = $('div#chat div.panel-body');

if (!util.supports.data) {
	alert("К сожалению ваш браузер не поддерживается. Попробуйте обновить браузер.");
}

$('div#chat, footer, div.alert').hide();

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('a[data-toggle="popover"]').popover();
  var currentUser = localStorage.getItem('p2pby-user');
  if (currentUser && currentUser !== '' && currentUser !== null)
  {
	  $('input#inputLogin').val(currentUser);
  }
});

var profile = {};
var token;
var selectedUser;
var bannedUser;

usersContainer.on('click', 'a.user', function(e){
	e.preventDefault();
	usersContainer.find('a.user').removeClass('active');
	var id = $(this).parent().attr('data-id');
	var login = $(this).text();
	if (selectedUser && selectedUser.id == id)
	{
		selectedUser = undefined;
	}
	else {
		$(this).addClass('active');
		selectedUser = {id: id, login: login};
	}
});


usersContainer.on('click', 'a.banned', function(e){
	e.preventDefault();
	usersContainer.find('a.user').removeClass('active').removeClass('banned');
	var login = $(this).parent().find('a.user').text();
	if (bannedUser.hasOwnProperty(login))
	{
		delete bannedUser[login];
		localStorage.setItem('p2pby-banned-users'+profile.login, JSON.stringify(bannedUser));
	}
	else {
		$(this).parent().find('a.user').addClass('banned');
		bannedUser[login] = '';
		localStorage.setItem('p2pby-banned-users'+profile.login, JSON.stringify(bannedUser));
	}
});

$('form[action="login"]').on('submit', function(e){
	e.preventDefault();
	var self = $(this);
	profile = {
		id: uuid(),
		login: self.find('input#inputLogin').val(),
		password: self.find('input#inputPassword').val(),
		sex: self.find('input[name=inputSex]:checked').val(),
		recaptcha: document.getElementById("g-recaptcha-response").value
	};
	$.post('/login', profile)
	.done(function (result) {
		token = result.token;
		profile.sex = result.sex;
		delete profile.password;
		delete profile.recaptcha;
		createPeer(token);
		$('div#chat, footer').show();
		$('div#login, div#news').hide();
		localStorage.setItem("p2pby-user", profile.login);
		bannedUser = localStorage.getItem('p2pby-banned-users'+profile.login);
		if (bannedUser == null) {
			bannedUser = {};
		}
		else {
			bannedUser = JSON.parse(bannedUser);
		}
	})
	.fail(function(result){
		$('form[action="login"]').find('span:last-child').remove();
		$('form[action="login"]').find('div.alert').show().append('<span>'+result.responseJSON.error+'</span>');
		grecaptcha.reset();
	});
});

$('textarea').keyup(function (event) {
    if (event.keyCode == 13) {
        var content = this.value;  
        var caret = getCaret(this);          
        if(event.shiftKey){
            this.value = content.substring(0, caret - 1) + "\n" + content.substring(caret, content.length);
            event.stopPropagation();
        } else {
            this.value = content.substring(0, caret - 1) + content.substring(caret, content.length);
            sendMessage();
        }
    }
});

function getCaret(el) { 
    if (el.selectionStart) { 
        return el.selectionStart; 
    } else if (document.selection) { 
        el.focus();
        var r = document.selection.createRange(); 
        if (r == null) { 
            return 0;
        }
        var re = el.createTextRange(), rc = re.duplicate();
        re.moveToBookmark(r.getBookmark());
        rc.setEndPoint('EndToStart', re);
        return rc.text.length;
    }  
    return 0; 
}

$(window).on('hashchange', function(){
	$('#navbar').find('li').removeClass('active');
	$('#navbar').find('li span').text('');
	var hash = location.hash;
	var sel_room= $('#navbar').find('a[href="'+hash+'"]');
	if (sel_room.length > 0)
	{
		sel_room.parent().addClass('active');
	}
	if (hash !== '#'+ ROOM)
	{
		ROOM = hash.substr(1);
		createPeer(token);
	}
});

function truncChat() {
	var messages = chatlog.find('>p');
	if (messages.length > 50)
	{
		chatlog.find('>p:lt('+(messages.length-50)+')').remove();
	}
}

function getChartMessageElement(login, message, isPrivat){
	var chat_message = $($('script#chat-message')[0].innerHTML);
	if (isPrivat)
	{
		chat_message.addClass('bold');
	}
	chat_message.find('span.peer>a.user').text(login);
	chat_message.find('span.text').text(message);
	return chat_message;
}

function recalculateUserCountInRoom()
{
	var selCount = $('#navbar').find('li.active span');
	if (selCount.length > 0)
	{
		var count = usersContainer.find('>p').length;
		selCount.text(count);
	}
}

function uuid() {
    var s4 = function () {
        return Math.floor(Math.random() * 0x10000).toString(16);
    };
    return s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4();
}

function getUserHtmlElemant(id, login, sex){
	var user_templ = $($('script#user-template')[0].innerHTML);
	user_templ.find('a.user').text(login);
	user_templ.find('span.label').text(sex);
	user_templ.attr('data-id', id);
	return user_templ;
}

var ROOM = location.hash.substr(1);
if (!ROOM) {
    ROOM = 'general';
}

$('#navbar').find('li a').each(function(){
	if ($(this).attr('href') === '#'+ROOM)
	{
		$(this).parent().addClass('active');
	}
});

custom_room.href = "#"+ uuid();

var users = {};
var peer;
var socket;

function createPeer(token){
	usersContainer.html('');
	if (!socket)
	{
		socket = io.connect("", {"sync disconnect on unload": true, 'query': 'token=' + token});
		socket.on("new", socketNewPeer);
	}
	socket.emit("room", {room: ROOM});
	if (peer)
	{
		peer.disconnect();
		peer.destroy();
	}
	peer = new Peer(profile.id, {path: '/peers-api', port: 8000, host:'/'});
	peer.on('open', function() {
		if (usersContainer.find('p[data-id="'+profile.id+'"]').length == 0)
		{
			usersContainer.append($('<p data-id="'+profile.id+'">'+profile.login+'&nbsp;<span class="label label-primary">'+profile.sex+'</span></p>'));
			recalculateUserCountInRoom();
		}
	});
	peer.on('connection', connect);
	peer.on('disconnected', function(){
		if (peer.disconnected)
		{
			createPeer();
		}
	});
}

function socketNewPeer(data){
	var newUser = data;
	if (!users[newUser.id])
	{
		users[newUser.id] = newUser;
		var c = peer.connect(newUser.id, {
			serialization: 'none',
			reliable: false,
			metadata: {sender: profile, receiver: newUser}
		});
		c.on('open', function() {
			usersContainer.append(getUserHtmlElemant(newUser.id, newUser.login, newUser.sex));
			if (bannedUser.hasOwnProperty(newUser.login)){
				usersContainer.find('p[data-id="'+newUser.id+'"]').find('a.user').addClass('banned');
			}
			if (selectedUser && selectedUser.id == newUser.id)
			{
				usersContainer.find('p[data-id="'+newUser.id+'"]').find('a.user').addClass('active');
			}
			recalculateUserCountInRoom();
		});
		c.on('close', function(){
			usersContainer.find('p[data-id="'+newUser.id+'"]').remove();
			recalculateUserCountInRoom();
			delete users[c.peer];
		});
		c.on('data', function(data){
			var mesObj = JSON.parse(data);
			if (mesObj.msg !== '' && !bannedUser.hasOwnProperty(c.metadata.receiver.login))
			{
				chatlog.append(getChartMessageElement(c.metadata.receiver.login, mesObj.msg, mesObj.privat));
				truncChat();
				$('html, body').scrollTop($(document).height());
			}
		});
		c.on('error', function(err) { alert(err); });
	}
}

function connect(c){
	c.on('open', function(){
		usersContainer.append(getUserHtmlElemant(c.metadata.sender.id, c.metadata.sender.login, c.metadata.sender.sex));
		if (bannedUser.hasOwnProperty(c.metadata.sender.login)){
			usersContainer.find('p[data-id="'+c.metadata.sender.id+'"]').find('a.user').addClass('banned');
		}
		if (selectedUser && selectedUser.id == c.metadata.sender.id)
		{
			usersContainer.find('p[data-id="'+c.metadata.sender.id+'"]').find('a.user').addClass('active');
		}
		recalculateUserCountInRoom();
		users[c.metadata.sender.id] = {id: c.metadata.sender.id, login: c.metadata.sender.login, sex: c.metadata.sender.sex};
	});
	c.on('close', function(){
		usersContainer.find('p[data-id="'+c.metadata.sender.id+'"]').remove();
		recalculateUserCountInRoom();
		delete users[c.peer];
	});
	c.on('data', function(data){
		var mesObj = JSON.parse(data);
		if (mesObj.msg !== '' && !bannedUser.hasOwnProperty(c.metadata.sender.login))
		{
			chatlog.append(getChartMessageElement(c.metadata.sender.login, mesObj.msg, mesObj.privat));
			truncChat();
			$('html, body').scrollTop($(document).height());
		}
	});
	c.on('error', function(err) { alert(err); });
}

function eachActiveConnection(fn) {
	var conns;
	var conn;
	if (selectedUser && users[selectedUser.id])
	{
		conns = peer.connections[selectedUser.id];
		for (var i = 0, ii = conns.length; i < ii; i += 1) {
			conn = conns[i];
			fn(conn);
		}
	}
	else {
		for (var selUser in users) {
	  		if (users.hasOwnProperty(selUser)) {
				conns = peer.connections[selUser];
				for (var i = 0, ii = conns.length; i < ii; i += 1) {
					conn = conns[i];
					fn(conn);
				}
	  		}
		}
	}
}

function closeAllConnections()
{
	var conns;
	var conn;
	for (var selUser in users) {
  		if (users.hasOwnProperty(selUser)) {
			conns = peer.connections[selUser];
			for (var i = 0, ii = conns.length; i < ii; i += 1) {
				conn = conns[i];
				conn.close();
			}
  		}
	}
}

window.addEventListener("beforeunload", closeAllActiveConnection);
send_btn.addEventListener("click", sendMessage);

function closeAllActiveConnection(){
	closeAllConnections();
}

function sendMessage () {
	var msg = message.value;
	if (msg !== '')
	{
		eachActiveConnection(function(c){
			c.send(JSON.stringify({msg: msg, privat: selectedUser && users[selectedUser.id]}));
		});
		if (selectedUser && users[selectedUser.id])
		{
			chatlog.append("<p class='bold'>"+profile.login +": " + msg + "</p>");
		}
		else{
			chatlog.append("<p>"+profile.login +": " + msg + "</p>");
		}
		$('html, body').scrollTop($(document).height());
		message.value = "";
		message.focus();
	}
}
